import { resolve } from 'path'

const js = 'src/js/'
const preact = 'src/preact/'
const style = 'src/style/'

const inputs = {

  code: resolve(__dirname, style+'code.scss'),
  copy: resolve(__dirname, js+'code.js'),
  twig_components: resolve(__dirname, style+'twig_components.scss'),
}

export default inputs