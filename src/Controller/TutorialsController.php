<?php

namespace Drupal\dsfr_twig_components\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\dsfr_twig_components\Twig\PseudoComponents;
use Drupal\dsfr_twig_components\Twig\Resources;

/**
 * 
 */
class TutorialsController extends ControllerBase {

  protected $formBuilder;

  /**
   * {@inheritdoc}
   */
  public function __construct(FormBuilderInterface $form_builder ) {
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function index( $slug ) {

    $dsfr_url = 'https://www.systeme-de-design.gouv.fr/';
    $d_comp = 'composants-et-modeles/composants/';
    $dsfr_drupal = 'https://www.drupal.org/project/dsfr';

    // Sidemenu
    // ----------------------------------------------------------------------------------------- //

    $a = 'link';
    $sidemenu = [
      'home' => $this->sideMenu([]),
      'how'  => $this->sideMenu(['How DSFR Components work?', 'how', $a ]),
      'edition' => $this->sideMenu(['Content edition', null, 'button', '', true]),
      'collection' => $this->sideMenu(['Collection (edition)', null, 'button', '', true]),
      'alert' => $this->sideMenu(['Informative', null, 'button', '', true]),
      'form' => $this->sideMenu(['Form', null, 'button', '', true]),
      'other' => $this->sideMenu(['Other DSFR components', null, 'button', '', true]),
    ];
    $components_ready = Resources::functionsReady();
    foreach($components_ready as $row) $submenu[$row['name']] = $this->subMenu( $row );

    // Content
    // ----------------------------------------------------------------------------------------- //


    $lorem = 'Lorem ipsum';

    $mt = 'fr-mt-5v';
    $mb = 'fr-mb-5v';
    $m_string = "'" . $mt . "', '" . $mb . "'";
    $m_array = '[' . $m_string . ']';

    $content = [

      'each' => $this->t('Each component includes:'),
      'result' => $this->t('The result is:'),

      'dsfr_url' => $dsfr_url,
      'dsfr_components' => $dsfr_url . $d_comp,
      'dsfr_theme' => $dsfr_drupal,
      'dsfr_paragraph' => $dsfr_drupal . '_paragraph',

      'lorem' => PseudoComponents::chevrons($lorem, 'strong') . ' dolor sit amet.',
      'text' => '<strong>' . $lorem . '</strong> dolor sit amet.',
      'label' => 'Label',
      
      'code_label' => 'code',
      'prefix' => 'dsfr_',
      'arg' => '( array data_config, array attributes )',
      'att' => "{'id': 'custom-id', 'class': " . $m_array . "}",

      'mt' => $mt,
      'mb' => $mb,
      'm' => [$mt, $mb],
      'm_string' => $m_string,
      'm_array' => $m_array,

      'color' => Resources::colorLine(),

      'end' => '<hr class="end" />',

      'functions' => $components_ready
    ];

    // ----------------------------------------------------------------------------------------- //

    return [
      '#theme'    => '_tutorials',
      '#content'  => $content,
      '#sidemenu' => $sidemenu,
      '#submenu' => $submenu,
      '#slug'     => $slug,
      '#filter_functions' => \Drupal::formBuilder()->getForm('Drupal\dsfr_twig_components\Form\FilterFunctionsForm')
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function sideMenu( array $row ) {

    return [  
      'label' => (isset($row[0])) ? $row[0] : $this->t('About these functions'),
      'url'   => (isset($row[1])) ? $row[1] : '',
      'type'  => (isset($row[2])) ? $row[2] : 'link',
      'parent'=> (isset($row[3])) ? $row[3] : '',
      'open'  => (isset($row[4])) ? $row[4] : false,
      'slug'  => (isset($row[5])) ? $row[5] : '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function subMenu( array $row ) {

    $key = $row['name'];
    return [  
      'label' => ucfirst($key),
      'url'   => $row['url'],
      'parent'=> $row['parent'],
      'slug'  => $key,
    ];
  }
}