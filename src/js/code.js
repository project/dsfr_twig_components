(function ($, Drupal) {

  Drupal.behaviors.codeCopy = {
    attach: function (context, settings) {

      let blocksCode = document.querySelectorAll(".fr-code");

      if (navigator.clipboard) {

        blocksCode.forEach((block) => {

          let pre = block.querySelector('pre');
          let button = block.querySelector('.copy-code-button');
      
          button.addEventListener('click', function() {
    
            let codeText = pre.textContent;
          
            navigator.clipboard.writeText(codeText).then(() => {
    
              button.classList.add('copied');
              button.textContent = 'Code copied!';
    
            }).catch(err => {
                console.error('Copy error :', err);
            });
          });      
        });  
      }

      else {
            
        blocksCode.forEach((block) => {
          let button = block.querySelector('.copy-code-button');
          button.textContent = 'Copy unavailable';      
        }); 
      }
    }
  };
})(jQuery, Drupal);