<?php

namespace Drupal\dsfr_twig_components\Twig;

use Drupal\dsfr_twig_components\Twig\ExternalTools;
use Drupal\dsfr_twig_components\Twig\InternalTools;
use Drupal\dsfr_twig_components\Twig\Markup;
use Drupal\dsfr_twig_components\Twig\PseudoComponents;

/**
 * See: https://www.systeme-de-design.gouv.fr/composants-et-modeles/composants/
 */
class DsfrComponents {

  #########################################################################################################################
  # 1 # SET PARAMETERS 
  #########################################################################################################################

  public static function accordion( array $dc = [], array $a = [] ) {
    $component = self::configComponent('accordion', ['title', 'text']);
    $component['id'] = true;
    return self::dsfrComponent( $component, $dc, $a );
  } 
  public static function accordions( array $group, array $a = [] ) { 
    return self::dsfrComponentGroup( 'accordions', $group, $a );
  } 

  public static function alert( array $dc = [], array $a = [] ) {
    return self::dsfrComponent( self::configComponent('alert', [], ['type' => 'warning']), $dc, $a  );
  } 
  
  public static function badge( array $dc = [], array $a = [] ) {    
    return self::dsfrComponent( self::configComponent('badge'), $dc, $a );
  } 
  public static function badges( array $group, array $a = [] ) { 
    return self::dsfrComponentGroup( 'badges', $group, $a, 'li' ); 
  } 

  public static function button( array $dc = [], array $a = [] ) {    
    return self::dsfrComponent( self::configComponent('btn'), $dc, $a );
  } 
  public static function buttons( array $group, array $a = [] ) {
    return self::dsfrComponentGroup( 'btns', $group, $a, 'li' );
  } 

  public static function callout( array $dc = [], array $a = [] ) {    
    return self::dsfrComponent( self::configComponent('callout'), $dc, $a  );
  } 

  public static function card( array $dc = [], array $a = [] ) {
    return self::dsfrComponent( self::configComponent('card', ['title']), $dc, $a );
  } 

  public static function franceconnect( array $dc = [], array $a = [] ) { 
    return self::dsfrComponent( self::configComponent('franceconnect', [], []), $dc, $a  );  
  } 

  public static function highlight( array $dc = [], array $a = [] ) {
    return self::dsfrComponent( self::configComponent('highlight'), $dc, $a );
  }

  public static function image( array $dc = [], array $a = [] ) {

    return self::dsfrComponent( self::configComponent('image', ['src']), $dc, $a );
  }

  public static function link( array $dc = [], array $a = [] ) {
    return self::dsfrComponent( self::configComponent('link'), $dc, $a );
  }

  public static function notice( array $dc = [], array $a = [] ) {
    $component = self::configComponent('notice', ['start', 'end', 'title'], ['type' => 'info'] );
    $component['id'] = true;
    return self::dsfrComponent( $component, $dc, $a );
  }

  public static function quote( array $dc = [], array $a = [] ) {
    return self::dsfrComponent( self::configComponent('quote'), $dc, $a );
  }

  public static function search( array $dc = [], array $a = [] ) {
    $component = self::configComponent('search', [], []);
    $component['id'] = true;
    return self::dsfrComponent( $component, $dc, $a );
  }

  public static function tab( array $dc = [], array $a = [] ) {
    $component = self::configComponent('tab', ['title', 'text']);
    $component['id'] = true;
    return self::dsfrComponent( $component, $dc, $a );
  } 
  public static function tabs( array $group, array $a = [] ) { 
    return self::dsfrComponentGroup( 'tabs', $group, $a );
  } 

  public static function tag( array $dc = [], array $a = [] ) {
    return self::dsfrComponent( self::configComponent('tag'), $dc, $a );
  }
  public static function tags( array $group, array $a = [] ) {
    return self::dsfrComponentGroup( 'tags', $group, $a, 'li' );
  } 

  public static function tile( array $dc = [], array $a = [] ) {
    return self::dsfrComponent( self::configComponent('tile', ['title']), $dc, $a );
  } 

  public static function title( array $dc = [], array $a = [] ) {
    return self::dsfrComponent( self::configComponent('title', ['title']), $dc, $a );
  }

  public static function tooltip( array $dc = [], array $a = [] ) {
    $component = self::configComponent('tooltip');
    $component['id'] = true;
    return self::dsfrComponent( $component, $dc, $a );
  }
  
  #########################################################################################################################
  # 2 # COMPONENT HANDLING
  #########################################################################################################################

  public static function dsfrComponent( array $component, array $data_config = [], array $attributes = []) {

    $ok = true;
    $msg = 'at least one mandatory data item is missing.'; 
    $dontcheck = ['franceconnect', 'search'];

    if( count( $data_config ) > 0 || in_array($component['key'], $dontcheck )) {
      
      // ================================================================================================================= //      
      // 2.1 -- Check mantory field
      // ================================================================================================================= //
      ### alert is a special case
      if( $component['key'] == 'alert' ) { 

        $title = InternalTools::checkField('title', $data_config, '');
        $text = InternalTools::checkField('text', $data_config, '');
        $sm = InternalTools::checkField('sm', $data_config);

        if( $text == '' && $title == '' && $sm == false ) $ok = false;
        // if sm text is mandatory
        elseif( $text == '' && $sm == true ) {
          $ok = false; 
          $msg = 'in small mode (sm), descriptive text is mandatory or has no data.'; 
        }
        // if md title is mandatory
        elseif( $title == '' && $sm == false ) {
          $ok = false;
          $msg = 'in normal mode (md), title is mandatory or has no data.'; 
        }

      ### all other cases
      } elseif( count($component['check']) > 0 ) { 
        foreach( $component['check'] as $check ) {
          if( !array_key_exists( $check, $data_config )) $ok = false;
          elseif( $data_config[ $check ] == '' ) $ok = false; $msg = 'this component has no data.';  
        }
      }

      // ================================================================================================================= //
      // 2.2 -- Build DSFR component
      // ================================================================================================================= //

      if( $ok == true ) {

        $render = 'twig';

        // If attributes is a simple array => $attributes['class] is ready
        $attributes = InternalTools::checkClasses( $attributes );

        // GET Data + optional config
        // --------------------------------------------------------------------------------------------------------------- //
        extract( InternalTools::recurringVariables( $component['key'], $component['params'] ));     
        extract( $data_config );
        $size = [ 'sm' => $sm, 'lg' => $lg ];

        // When ID is mandatory but doesn't exist 
        // --------------------------------------------------------------------------------------------------------------- //
        if (  array_key_exists( 'id', $component ) 
              && $component['id'] == true 
              && !array_key_exists( 'id', $attributes ) ) $id = $key . '-' . ExternalTools::uniqId();
        elseif( array_key_exists( 'id', $attributes ) ) $id = $attributes['id'];
        
        // For each component
        // --------------------------------------------------------------------------------------------------------------- //
        $content = $config = [];

        switch( $component['key'] ) {

          case 'accordion': 
            $content = [
              'title' => internalTools::handleTra($title, $tra), 
              'text'  => internalTools::handleTra($text, $tra), 
              'id'    => $id ];
            break;

          case 'alert': 
            $content = [
              'title' => internalTools::handleTra($title, $tra), 
              'text'  => internalTools::handleTra($text, $tra), 
            ];
            $pattern = internalTools::setType( $key, $type, $pattern );
            $pattern = internalTools::setSize( $key, $size, $pattern );  
            $config['close'] = $close;
            break;

          case 'badge': $content['text'] = internalTools::handleTra($text, $tra);
            $pattern = internalTools::setType( $key, $type, $pattern );
            $pattern = internalTools::setColor( $key, $color, $pattern );
            $pattern = internalTools::setNoIcon( $key, $noicon, $pattern );
            $pattern = internalTools::setSize( $key, $size, $pattern );
            break;   

          case 'btn': 
            $content['text'] = internalTools::handleTra($text, $tra);
            $attributes = internalTools::setDisabled( $disabled, $attributes );
            if( $icon != '' ) {
              $k = 'fr-btn';
              if( $left == true ) { $pattern['class'] = internalTools::pattern( $k, 'icon-left', $pattern ); }
              elseif( $right == true ) { $pattern['class'] = internalTools::pattern( $k, 'icon-right', $pattern ); }
              $pattern = internalTools::setIcon( $icon, $pattern );
            }
            $pattern = internalTools::setSize( $key, $size, $pattern );
            if ( $type == 'no-outline') $type = 'tertiary-no-outline';

            $pattern = internalTools::setType( $key, $type, $pattern );
            break;

          case 'callout':
            $content = [
              'title' => internalTools::handleTra($title, $tra), 
              'text'  => internalTools::handleTra($text, $tra), 
              // The button must first be created EITHER by dsfr_button() with {[...], 'render': true}
              // OR - not recommanded - dsfr_link() with {[...], 'type': 'button', 'render': true} in $data_config()
              'button'=> InternalTools::checkField('button', $data_config, '') 
            ];
            $pattern = internalTools::setColor( $key, $color, $pattern );
            if( $noicon == false && $icon == '' ) $pattern['class'] = array_merge( $pattern['class'], ['fr-icon-information-line'] );
            elseif( $noicon == false && $icon != '' ) $pattern = internalTools::setIcon( $icon, $pattern );
            break;

          case 'card': 
            $title = internalTools::handleTra($title, $tra);
            $data_config = internalTools::typeUrl( $data_config ); // URL is an object?
            if($data_config['url'] != '') {
              $att_link = internalTools::buildHref( $data_config );
              $title = Markup::mA($data_config['title'], $att_link);
              array_push($pattern['class'], 'fr-enlarge-link');
            }
            $content = [
              'title' => $title, 
              'text'  => internalTools::handleTra($text, $tra),
              'img'   => $img,
              'alt'   => $alt,
              'one'   => $one
            ];
            break;
          
          case 'franceconnect': 
            $content['plus'] = ( array_key_exists( 'plus', $data_config ) && $data_config['plus'] == true ) ? '+' : '';
            break;
  
          case 'highlight': $content['text'] = internalTools::handleTra($text, true); // $tra);
            $pattern = internalTools::setColor( $key, $color, $pattern );
            $subpattern = internalTools::setSize( 'fr-text', $size, $subpattern );
            $config['size'] = ( count($subpattern['class']) > 0 ) ? InternalTools::convertAttributes( $subpattern ) : ''; 
            break;

          case 'image': 
            $pattern['class'] = ['fr-content-media'];
            $attributes['role'] = 'group';        
            $content = [
              'src'   => $src, 
              'alt'   => $alt, 
              'ratio' => $ratio,
            ];
            // legend    
            if( array_key_exists( 'text', $data_config )) {
              $legend = internalTools::handleTra($text, $tra);
              $attributes['aria-label'] = $legend;
              $content['text'] = $legend;
            }  
            break;

          case 'link': $content['text'] = $data_config['text'] = internalTools::handleTra($text, $tra);
            // LINK TYPE
            // ******************************************************************************************** //
            $config['type'] = InternalTools::checkField('type', $data_config, 'link');
            $data_config = internalTools::typeUrl( $data_config ); // URL is an object?
            if( array_key_exists('type', $data_config)) {
              if( $data_config['type'] == 'text') { 
                $config['type'] = 'text'; 
                $pattern['class'] = []; 
                $data_config['nolink'] = true;
              } else {
                $config['type'] = $data_config['type'];
                if( $data_config['type'] == 'real_button') $data_config['nolink'] = true;
                elseif( $data_config['type'] == 'button') {
                  $key = 'fr-btn';
                  $pattern['class'] = [$key];
                }
              }
            }
            // ******************************************************************************************** //
            $attributes = internalTools::buildHref( $data_config, $attributes );
            $pattern = internalTools::setSize( $key, $size, $pattern );

            if( $tooltip != '') {
              $content['tooltip'] = $tooltip;
              if( !array_key_exists('id', $attributes)) $attributes['id'] = $key . '-' . ExternalTools::uniqId();
              $attributes['aria-describedby'] = $content['id'] = 'tooltip-' . ExternalTools::uniqId(); 
            }

            if( $arrow == true && $right == true ) array_push($pattern['class'], 'fr-icon-arrow-right-line', $key.'--icon-right' );
            elseif( $arrow == true ) array_push($pattern['class'], 'fr-icon-arrow-right-line', $key. '--icon-left' );      
            break;

          case 'notice': 
            // HANDLE TIME
            // ******************************************************************************************** //    
            $ok = false;       
            $now = time();
            if( $now >= $start && $now <= $end ) {
              $ok = true;
              $content = [
                'title' => internalTools::handleTra($title, $tra), 
                'text'  => internalTools::handleTra($text, $tra), 
                'id'    => $id
              ];
              if( $url != '' ) $content['url'] = str_replace('fr-link', 'fr-notice__link', $url);
              $pattern = internalTools::setType( $key, $type, $pattern );
              $config['close'] = $close;
            }
            // ******************************************************************************************** //
            break;

          case 'quote': 
            $config['figcaption'] = false;
            if( $author != '' ) $author = Markup::mP( $author, ['class' => [$key.'__author']]); $config['figcaption'] = true;
            if( array_key_exists( 'details', $data_config ) && count($data_config['details']) > 0) {  
              $config['figcaption'] = true;  
              foreach ( $data_config['details'] as $item ) {
                if(array_key_exists( 'url', $item )) {
                  $item['type'] = 'link';
                  $item = internalTools::typeUrl( $item ); // URL is an object?
                  if( $item['type'] != 'link' ) $get_details[]['item'] = $item['text']; // <nolink>
                  else {!
                    $att_link_quote = internalTools::buildHref( $item );
                    $get_details[]['item'] = Markup::mA($item['text'], $att_link_quote); 
                  }
                } else { $get_details[]['item'] = $item['text'];  }
              }              
            } else { $get_details = []; };
            $content = [
              'image'   => $img, 
              'alt'     => $alt, 
              'text'    => internalTools::handleTra($text, $tra), 
              'author'  => $author, 
              'cite'    => $cite,
              'details' => $get_details
            ];
            array_push( $pattern['class'], $key.'--column' );
            $pattern = internalTools::setColor( $key, $color, $pattern );
            break;   
            
          case 'search': 
            $key = 'fr-search-bar';
            $content = [
              'text'         => ($text == '') ? internalTools::tra('Search') : internalTools::handleTra($text, $tra),
              'placeholder'  => $placeholder,
              'id'           => $id
            ];
            $pattern['class'] = [$key];
            $pattern = internalTools::setSize( $key, $size, $pattern );
            $attributes['role'] = 'search';
          break;

          case 'tab': $render = 'array';
            $content = [
              'title' => internalTools::handleTra($title, $tra), 
              'text'  => internalTools::handleTra($text, $tra), 
              'id'    => $id ];
            break;

          case 'tag': $content['text'] = internalTools::handleTra($text, $tra);
            $config['type'] = 'text';
            // TAG TYPE
            // ******************************************************************************************** //
            if( array_key_exists( 'url', $data_config)) {
              $data_config = internalTools::typeUrl( $data_config ); // URL is an object?
              $config['type'] = $data_config['type'];
              $attributes = internalTools::buildHref( $data_config, $attributes );
            } 
            elseif( array_key_exists( 'pressed', $data_config )) {
              $config['type'] = 'button'; 
              $attributes['aria-pressed'] = ( $data_config['pressed'] == true ) ? 'true' : 'false';
            }
            elseif( array_key_exists( 'close', $data_config )) {   
              $config['type'] = 'button';
              array_push($pattern['class'], $key.'--dismiss' );
              $attributes['aria-label'] = InternalTools::tra('Remove') . ' ' . $text;
              $attributes['onclick'] = "event.preventDefault(); this.parentNode.removeChild(this);";
            }            
           // ******************************************************************************************** //
            $pattern = internalTools::setSize( $key, $size, $pattern );
            if( $arrow == true ) array_push($pattern['class'], 'fr-icon-arrow-right-line', $key.'--icon-left' );
            break;

          case 'tile': 
            $title = internalTools::handleTra($title, $tra);
            $data_config = internalTools::typeUrl( $data_config ); // URL is an object?
            if($data_config['url'] != '') {
              $att_link = internalTools::buildHref( $data_config );
              $title = Markup::mA($data_config['title'], $att_link);
              array_push($pattern['class'], 'fr-enlarge-link');
            }
            $content = [
              'title' => $title, 
              'text'  => internalTools::handleTra($text, $tra),
              'img'   => $img,
              'alt'   => $alt,
              'one'   => $one
            ];
            break;
          
          case 'title': 
            $title = internalTools::handleTra($title, $tra);
            $type = internalTools::checkNumberTitle( $type );
            $h = ( $type != '' ) ? 'h' . $type : 'h2';
            $get_style = ( array_key_exists('style', $data_config)) ? internalTools::checkNumberTitle($data_config['style']) : '';
            $style = ( $get_style != '' ) ? ['fr-h' . $get_style] : [];
            $att_title = $attributes;
            $pattern['class'] = $style;
            if( $icon != '' ) $pattern = internalTools::setIcon( $icon, $pattern );
            $att_title['class'] = InternalTools::mergeClass( $pattern, $att_title );
            $content['title'] = Markup::item( $title, $att_title, $h, false);
            break;

          case 'tooltip': $content = [
              'text'  => internalTools::handleTra($text, $tra),
              'id'    => $id,
              'info'  => ( array_key_exists('style', $data_config )) ? $data_config['info'] : 'Contextual information',
            ];
            $pattern['class'] = ['fr-btn--tooltip', 'fr-btn'];
            $attributes['aria-describedby'] = $id;
            break;

          default: 
            $newkey = 'alert';
            $component['key'] = $newkey;
            $content['text'] = InternalTools::tra("This object doesn't exist!");
            $pattern['class'] = ['fr-'.$newkey, 'fr-'.$newkey.'--error', 'fr-'.$newkey.'--sm'];
            $config['close'] = false;
        }

        // =============================================================================================================== //
        // 2.3 -- Render (see below: Twig rendering)
        // =============================================================================================================== //

        // Handle attributes
        // --------------------------------------------------------------------------------------------------------------- //
        $attributes['class'] = InternalTools::mergeClass( $pattern, $attributes );
        $attr_string = InternalTools::convertAttributes( $attributes );

        // Final content (template twig for each component)
        // --------------------------------------------------------------------------------------------------------------- //
        if( $render != 'array') $render = InternalTools::checkField('render', $data_config, 'twig');
        if( $ok == true ) return self::render( $component['key'], $content, $attributes, $attr_string, $config, $render );  

      } else { return PseudoComponents::cIncorrect( $msg ); }  
    } else { return PseudoComponents::cIncorrect( $msg ); }   
  }

  public static function configComponent( string $key, array $check = ['text'], array $params = [] ) {
    return [
      'key'     => $key,      // component's key
      'check'   => $check,    // mandatory fields
      'params'  => $params,   // see: internalTools::configComponent()
    ];
  }

  public static function dsfrComponentGroup( 
    string $key, 
    array $data_config = [], 
    array $attributes = [], 
    string $tag = '' ) {
 
    $ok = true;
    $msg = 'at least one mandatory component is missing.';
    $merged_content = '';
    $key_fr = 'fr-'. $key .'-group';
    $group = ( array_key_exists('group', $data_config) ) ? $data_config['group'] : [];

    if( count($group) > 0 ) {

      // If attributes is a simple array => $attributes['class] is ready
      $attributes = InternalTools::checkClasses( $attributes );

      // ------------------------------------------------------------------------------------------------------------------- //
      if( $key != 'tabs' ) {

        $pattern['class'] = [ $key_fr ];
        
        // special case: buttons
        if( $key == 'btns' ) {
          // Horizontl or vertical (not full width)
          if( array_key_exists('inline', $data_config) && $data_config['inline'] == true ) {
  
            if(array_key_exists('sm', $data_config) && $data_config['sm'] == true ) {
              $pattern['class'] = internalTools::pattern( $key_fr, 'inline-sm', $pattern );
            }
            elseif(array_key_exists('md', $data_config) && $data_config['md'] == true ) { 
              $pattern['class'] = internalTools::pattern( $key_fr, 'inline-md', $pattern );
            }
            elseif(array_key_exists('lg', $data_config) && $data_config['lg'] == true ) { 
              $pattern['class'] = internalTools::pattern( $key_fr, 'inline-lg', $pattern );
            }
            else $pattern['class'] = internalTools::pattern( $key_fr, 'inline', $pattern );
  
          } elseif( array_key_exists('full-width', $data_config)) {      
            $pattern['class'] = internalTools::pattern( $key_fr, 'equisized', $pattern );
          } elseif( array_key_exists('equisized', $data_config)) {
            $pattern['class'] = internalTools::pattern( $key_fr, 'equisized', $pattern );
          } 
          // Position
          if(array_key_exists('left', $data_config) && $data_config['left'] == true ) {
            $pattern['class'] = internalTools::pattern( $key_fr, 'left', $pattern );
          }
          elseif(array_key_exists('center', $data_config) && $data_config['center'] == true ) { 
            $pattern['class'] = internalTools::pattern( $key_fr, 'center', $pattern );
          }
          elseif(array_key_exists('right', $data_config) && $data_config['right'] == true ) { 
            $pattern['class'] = internalTools::pattern( $key_fr, 'right', $pattern );
          }
        } 

        foreach( $group as $item ) {
          $item = ( $tag != '' )? Markup::item( $item, [], $tag) : $item;
          $merged_content .= $item;
        }
      }
      else { // tabs
        $list = $panel = '';
        $n = 0;
        $key_fr = 'fr-tabs';
        foreach( $data_config['group'] as $tab => $row ) {

          $id = $row[0]['id'];
          if ( $n == 0 ) { $tabindex =  '0'; $selected = 'true'; } else { $tabindex = '-1'; $selected = 'false'; }
          $n++;

          $list.='<li role="presentation">
            <button id="'.$id.'" class="fr-tabs__tab fr-icon-checkbox-line fr-tabs__tab--icon-left" tabindex="'.$tabindex.'" role="tab" aria-selected="'.$selected.'" aria-controls="tabpanel-'.$id.'">'.$row[0]['title'].'</button>
          </li>';
          $panel.='<div id="tabpanel-'.$id.'" class="fr-tabs__panel" role="tabpanel" aria-labelledby="'.$id.'" tabindex="0">
            '.$row[0]['text'].'
          </div>';
        }
        $merged_content = ['list' => $list, 'panel' => $panel ];
        $pattern['class'] = [ $key_fr ];
      }
    }
    else $ok = false;

    // Handle attributes
    // ------------------------------------------------------------------------------------------------------------------- //
    $attributes['class'] = InternalTools::mergeClass( $pattern, $attributes );
    $attr_string = InternalTools::convertAttributes( $attributes ); 
    
    // Final content (template twig for each component)
    // ------------------------------------------------------------------------------------------------------------------- //
    if( $ok == true ) return self::render( $key, ['group' => $merged_content], $attributes, $attr_string ); 
    else return PseudoComponents::cIncorrect( $msg );
  }

  #########################################################################################################################
  # 3 # TWIG RENDERING
  #########################################################################################################################
  
  public static function render( 
    string $key, 
    array $content = [], 
    array $a = [], string $a_str = '', 
    array $conf = [],
    string $render = 'twig' ) {

    $variables = [
      'theme'    => $key,     // see: twig_dsfr_components.module
      'content'  => $content,
      'attr'     => $a,       // attributes (array)
      'attr_str' => $a_str,   // attributes (string)
      'config'   => $conf     // optional elements
    ];    

    // Result
    if( $render == 'twig' ) return \Drupal::service('twig')->render( 'dsfr-comp-' . $key . '.html.twig', $variables ); 
    elseif( $render == 'array' ) return [ $content, $a, $a_str, $conf ];
    else { // Get variables for a module theme
      foreach( $variables as $k => $value ) $build['#'.$k] = $value;      
      return $build;
    }
  }
}