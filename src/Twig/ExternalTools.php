<?php

namespace Drupal\dsfr_twig_components\Twig;

use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Symfony\Component\Mime\MimeTypes;

class ExternalTools {

  /**
   * Array Title (ati)
   * @param string $value
   * @return array<string, string>
   */
  public static function ati(string $value): array { return [ 'title' => $value ]; }

  /**
   * Array Title+Text (atix)
   * @param string $value
   * @param string $value2
   * @return array<string, string>
   */
  public static function atix(string $value, string $value2): array { return [ 'title' => $value, 'text' => $value2 ]; }

  /**
   * Array Text (atx)
   * @param string $value
   * @return array<string, string>
   */
  public static function atx(string $value): array { return [ 'text' => $value ]; }

  /**
   * {@inheritdoc}
   * @param array $data
   * @param mixed $field
   * @param string $pattern
   * @param string|null $key
   * @param bool $boolean
   * @return array
   */
  public static function notEmptyMerge(
    array $data = [], 
    $field = NULL, 
    string $pattern = '', 
    $key = NULL,
    bool $boolean = true    
  ): array 
  { 
  
    if( is_numeric( $field ) ) {

      if( $boolean == true ) {
        $data[$key] = ( $field == 0 ) ? false :  true;
      } else { $data[$key] = $field; }
      
    } elseif( $field != NULL  ) {

      if( $key != NULL ) { $data[$key] = $field; }
      elseif( $pattern != '' ) { $data[] = $pattern . $field; }
    }
    return $data;
  }

  /**
   * Convert a file size to make it more readable
   * @param int $octet
   * @return string
   */
  public static function convertBytes(int $octet): string {

    $unite = array('octet', 'ko', 'mo', 'go');
  
    if ($octet < 1000) // octet
    {
      return $octet . $unite[0];
    } else {
      if ($octet < 1000000) // ko
      {
        $ko = round($octet / 1024, 2);
        return $ko . $unite[1];
      } else // Mo ou Go
      {
        if ($octet < 1000000000) // Mo
        {
          $mo = round($octet / (1024 * 1024), 2);
          return $mo . $unite[2];
        } else // Go
        {
          $go = round($octet / (1024 * 1024 * 1024), 2);
          return $go . $unite[3];
        }
      }
    }
  }

  /**
   * Get file information from a media ID
   * @param int $mid
   * @return array<string, string>
   */
  public static function getMediaInfo( int $mid ): array {

    $media = Media::load($mid);
    $fid = $media->getSource()->getSourceFieldValue($media);
    $file = File::load($fid);
    $mime = $file->getMimeType();

    $type = New MimeTypes();
    $ext = $type->getExtensions( $mime );

    return [
      'name' => $media->getName(),
      'url' => $file->createFileUrl(),
      'size' => self::convertBytes( $file->getSize() ),
      'mime' => $mime,
      'ext' => strtoupper( $ext[0] ),
    ];
  }

  /**
   * Limiting a number of characters
   * @param string $str
   * @param int $limit
   * @param string $after
   * @return string
   */
  public static function limitString(
    string $str = '', 
    int $limit = 60, 
    string $after = '[...]'): string {

    return ( strlen($str) > $limit ) ? mb_substr( $str, 0, $limit, 'UTF-8' ) . $after : $str;
  }

  /**
   * Provides a unique identifier
   * @return string
   */
  public static function uniqId(): string { return uniqid(); }

  /**
   * @param int $width
   * @param int $height
   * @return string
   */
  public static function calcImageRatio( int $width, int $height ): string {
    
    // width and height must be positive integers
    $width = abs((int)$width);
    $height = abs((int)$height);

    if ($width == 0 || $height == 0) return "16x9";

    $standardRatios = [
      "16x9/2" => 16/18,
      "16x9" => 16/9,
      "4x3" => 4/3,
      "3x2" => 3/2,
      "1x1" => 1,
      "3x4" => 3/4,
      "2x3" => 2/3
    ];

    // Real ratio
    $actualRatio = $width / $height;

    $closestRatio = null;
    //$closestRatio = '16x9';
    $minDifference = PHP_FLOAT_MAX;

    foreach ($standardRatios as $name => $ratio) {

      $difference = abs($actualRatio - $ratio);

      if ($difference < $minDifference) {
        
        $minDifference = $difference;
        $closestRatio = $name;
      }
    }

    return $closestRatio;
  }

  /**
   * Calculate the GCD (Greatest Common Divisor)
   * @param int $a
   * @param int $b
   * @return int
   */
  public static function gcd( int $a, int $b ): int {

    while ($b != 0) {
      $temp = $b;
      $b = $a % $b;
      $a = $temp;
    }
    return $a;
  }
}
