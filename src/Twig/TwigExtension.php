<?php

namespace Drupal\dsfr_twig_components\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Adds Twig functions
 */
class TwigExtension extends AbstractExtension
{
  /**
   * List of added functions
   * @return array<string, array{class: string, method: string}>
   */
  public function twigFunctionsList(): array {

    $functions = [];

    $f_dsfr = [ 
      // blocks (content edition)
      'highlight', 'image', 'link', 'quote', 'title', // 'summary',
      // blocks with effects (dynamic blocks)
      'accordion', 'accordions', 'tab', 'tabs', //'modal',
      // collection
      'card', 'tile', 
      // form
      'button', 'buttons', 'franceconnect', 'search', 'tooltip', //'input', 'range', 'select',
      // notifications
      'alert', 'badge', 'badges', 'callout', 'notice',
      // short elements
      'tag', 'tags',                                           
    ];
    foreach( $f_dsfr as $f ) { $functions['dsfr_'.$f] = $this->set(DsfrComponents::class, $f);  }

    // Pseudo components "c_<name>"
    $pseudo = PseudoComponents::class;
    $f_pseudo = ['code', 'copy_code', 'error', 'info', 'hl', 'warning'];
    foreach( $f_pseudo as $f ) { $functions['c_'.$f] = $this->set($pseudo, 'c'.$this->rename($f));  }
    $f_pseudo = ['braces', 'chevron', 'chevrons', 'code_twig', 'comment_twig', 'var_twig'];
    foreach( $f_pseudo as $f ) { $functions[$f] = $this->set($pseudo, $this->rename($f));  }
    
    // Markup components (tag) "m_<name>"
    $markup = Markup::class;
    $functions['markup'] = $this->set($markup, 'item');
    $f_markup = ['a', 'btn', 'button', 'div', 'hr', 'img', 'p'];
    foreach( $f_markup as $f ) { $functions['m_'.$f] = $this->set($markup, 'm'.$this->rename($f));  }
    
    // External Tools
    $f_ext = ['ati', 'atix', 'atx', 'calc_image_ratio', 'convert_bytes', 'get_media_info', 'limit_string', 'not_empty_merge', 'uniq_id'];
    foreach( $f_ext as $f ) { $functions[$f] = $this->set(ExternalTools::class, lcfirst($this->rename($f)));  }
    
    return $functions;
  }

  /**
   * @return array<TwigFunction>
   */
  public function getFunctions(): array {

    foreach ($this->twigFunctionsList() as $name => $item) {

      $twig_functions[] = new TwigFunction(
        $name,
        [$item['class'], $item['method']],
        ['is_safe' => ['html']]
      );
    }
    
    return $twig_functions;
  }

  /**
   * {@inheritdoc}
   * @param string|object $class
   * @param string $method
   * @return array{class: string|object, method: string}
   */
  public function set( $class, string $method ): array {
    return ['class' => $class, 'method' => $method];
  }

  /**
   * {@inheritdoc}
   * @param string $method
   * @return string
   */
  public function rename( string $method ): string { 
    $final = '';
    $get = explode( '_', $method );
    foreach( $get as $value ) $final .= ucfirst( $value );
    return $final;
  }
}