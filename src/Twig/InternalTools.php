<?php

namespace Drupal\dsfr_twig_components\Twig;

use Drupal\Core\Url;

class InternalTools {

  /**
   * Recurring variables in most DSFR components
   */
  public static function recurringVariables( string $key = '', array $params = [] ) {

    $type = '';
    $color = '';
    $close = true;
    $noicon = false;   
    $arrow = false;
    $container = true; 
    $tra = false;
    $lg = false;
    $sm = false;
    $img = $svg = '';

    if( count($params) > 0 ) { extract( $params ); }

    $key = 'fr-' . $key;
    return [
  
      // Main
      'key'         => $key,
      'type'        => $type,
      'color'       => $color,
      'close'       => $close,

      // if sub-markup must change
      'container'   => $container, 

      // Icons
      'icon'        => '',
      'noicon'      => $noicon,
      'arrow'       => $arrow,
      'left'        => false,
      'right'       => false,
      
      // Media
      'ratio'       => '16x9',
      'alt'         => '',
      'img'         => $img,
      'svg'         => $svg,
      
      // Size
      'lg'          => $lg,
      'sm'          => $sm,

      // Translate available
      'tra'         => $tra, 

      // Text
      'author'      => '',
      #'description' => '',
      #'label'       => '',
      #'legend'      => '',
      'placeholder' => '',
      'title'       => '',
      'text'        => '',
      'tooltip'     => '',
      
      // Other
      'button'      => [],
      'cite'        => '', // link
      'disabled'    => false,
      'figcaption'  => '',
      'hidden'      => false,
      'id'          => null,
      'one'         => false,
      'pattern'     => ['class' => [$key] ],
      'subpattern'  => ['class' => [] ],
      'url'         => ''
    ];
  }

  /**
   * {@inheritdoc}
     * Twig functions available:
     * link() => HTML
     * path() => relative
     * url() => absolute
   */
  public static function buildHref( array $data_config, array $attributes = [] ) {

    $title = $url = '';
    $ext = $external = $nolink = false;
    $check = true;
    extract($data_config);

    if( $nolink == false ) {

      if( !array_key_exists( 'href', $attributes ) && $url != '' ) $attributes['href'] = $url;
      $attributes['href'] = ( array_key_exists('href', $attributes ) ) ? $attributes['href'] : '#';
  
      // Handle external url
      $attributes['target'] = '_self';
      $dont_check = ['/', '#'];
      if( $ext == true || $external == 'true' ) $attributes = self::setExternal( $attributes );
      elseif( !in_array($attributes['href'], $dont_check) && $check == true ) {
        // Script to detect url target
        $parsed_url = parse_url($url);
        if( isset( $parsed_url['host'] )) {
          $host = $parsed_url['host'];
          $external = ($host == $_SERVER['HTTP_HOST']) ? false : true;
        }     
        if( $external == true ) $attributes = self::setExternal( $attributes );
      }
    }

    if( array_key_exists('title', $data_config)) $attributes = self::setAttributesText( 'title', $title, $data_config['text'], $attributes );

    return $attributes;
  }
  
  /**
   * {@inheritdoc}
   */
  public static function typeUrl( array $data_config ) {

    $data_config['type'] = 'link'; 
    $url = array_key_exists('url', $data_config) ? $data_config['url'] : '';

    if ( is_array($url) ) { $url = Url::fromUri($url['uri']); }

    if ( is_object($url) ) { 

      $href = $url->toString();
      $data_config['url'] = $href;
      $data_config['external'] = $url->isExternal();
      
      if( $href == '' ) {
        
        switch( $url->getRouteName() ) {
  
          case '<button>':
            $data_config['type'] = 'button';
            break;
          
          case '<nolink>': 
            $data_config['type'] = 'text'; 
            break;
        }
      }
    } 
    return $data_config;
  }

  /**
   * Recurring key format for each component
   */
  public static function formatKey( string $key, string $type ) { return $key . '--' . $type; }
  
  /**
   * {@inheritdoc}
   */
  public static function checkClasses( array $attributes ) {
    
    $n = 0;
    $classes = $attr = $attr_rebuild = [];

    if( count($attributes) > 0 ) {

      foreach( $attributes as $key => $attribute ) { 

        if (is_int($key)) { 

          $n ++; 
          array_push( $classes, $attribute );

        } else { $attr[$key] = $attribute; }
      }
      $pattern['class'] = $classes;
      $attr_rebuild = $attr;
      $attr_rebuild ['class'] = self::mergeClass( $pattern, $attr_rebuild );
    }
    return $attr_rebuild;
  }

  /**
   * {@inheritdoc}
   */
  public static function checkField( string $key, array $data_config, $default = false ) {     
    return ( array_key_exists( $key, $data_config )) ? $data_config[$key] : $default;
  } 

  /**
   * {@inheritdoc}
   */
  public static function checkNumberTitle( $number ) {     
    if( $number != '' ) {
      if( !is_numeric( $number ) ) $number = 2;
      elseif( $number < 1 ) $number = 1;
      elseif( $number >= 7 ) $number = 6;
    }
    return $number;
  } 

  /**
   * {@inheritdoc}
   * Convert array ("Attributes") to string
   */
  public static function convertAttributes( array $attributes ) { 

    $attr = $classes = '';
    $n = 0;

    if( count($attributes) > 0 ) {
      
      foreach( $attributes as $key => $attribute ) { 
        
        $attr_array = false;  // attribute is an array
        $count_attr = 0;      // count attribute

        if ( is_array($attribute)) {

          $count_attr = count($attribute);
          $attr_array = true;
        }

        if (!is_int($key)) {

          // special case: class, rel, ...
          $special = [ 'class', 'rel' ];
          if( in_array( $key, $special ) &&  $attr_array == true && $count_attr > 0 ) { 

            $show_attribute = '';
            foreach( $attribute as $class ) { 
              if( is_string($class) ) { $show_attribute.= $class . ' '; }
            }
            $attr.= ' ' . $key . '="' . trim( $show_attribute ). '"'; 

          // others : id, aria, ... 
          } elseif( is_string( $attribute )) { 
            
            $show_attribute = $attribute; 
            $attr.= ' ' . $key . '="' . $show_attribute . '"'; 
          }
          $n++;
          
        } else { $classes.= ''. $attribute; }
      }

      if( $n == 0 ) $attr = ' class="' . trim($classes) . '"'; 
    }
    return $attr;
  }

  /**
   * {@inheritdoc}
   */
  public static function handleTra( string $content, bool $tra ) { 

    if( $tra == true ) $content = self::tra($content); 
    return $content;
  }
  
  /**
   * {@inheritdoc}
   */
  public static function mergeClass( array $pattern, array $attributes ) {
    $attributes['class'] = ( array_key_exists( 'class', $attributes) ) ? $attributes['class'] :  [];
    return array_merge( $pattern['class'], $attributes['class']  );
  }

  /**
   * {@inheritdoc}
   */
  public static function pattern( string $key, string $value, array $pattern ) { 
    return array_merge( $pattern['class'], [self::formatKey( $key, $value )] ); 
  }

  /**
   * {@inheritdoc}
   */
  public static function setAttributesText( string $key, $value, $text, array $attributes = [] ) {
    if( $value != '' && $value !== true  ) $attributes[$key] = $value;
    elseif( $value == true ) $attributes[$key] = $text;
    return $attributes;
  }

  /**
   * {@inheritdoc}
   */
  public static function setColor( $key, $color, $pattern ) {
    if ( $color != '' ) $pattern['class'] = self::pattern( $key, $color, $pattern );
    return $pattern;
  }

  /**
   * {@inheritdoc}
   */
  public static function setDisabled( $disabled, $attributes ) {
    if ( $disabled == true ) $attributes['disabled'] = 'disabled';
    return $attributes;
  }

  /**
   * {@inheritdoc}
   */
  public static function setExternal( $attributes ) {
    $attributes['target'] = '_blank';
    $attributes['rel'] = ['noopener', 'external'];
    return $attributes;
  }

  /**
   * {@inheritdoc}
   */
  public static function setIcon( $icon, $pattern, $key = 'fr-icon-' ) {
    if ( $icon == 'check' || $icon == 'default' ) 
      $pattern['class'] = array_merge( $pattern['class'], [ $key.'checkbox-circle-line' ]);
    elseif ( $icon != '' ) 
      $icon = ( str_starts_with($icon, $key)) ? $icon : $key.$icon; 
      $pattern['class'] = array_merge( $pattern['class'], [ $icon ]);
    return $pattern;
  }

  /**
   * {@inheritdoc}
   */
  public static function setNoIcon( $key, $noicon, $pattern ) {
    if ( $noicon == true ) $pattern['class'] = self::pattern( $key, 'no-icon', $pattern );
    return $pattern;
  }

  /**
   * {@inheritdoc}
   */
  public static function setSize( $key, $size, $pattern ) {
    if ( $size['sm'] == true ) $pattern['class'] = self::pattern( $key, 'sm', $pattern ); 
    elseif ( $size['lg'] == true ) $pattern['class'] = self::pattern( $key, 'lg', $pattern );  
    return $pattern;
  }

  /**
   * {@inheritdoc}
   */
  public static function setType( $key, $type, $pattern ) {
    if ( $type != '' ) $pattern['class'] = self::pattern( $key, $type, $pattern );
    return $pattern;
  }

  /**
   * {@inheritdoc}
   */
  public static function tra( string $content ) { 
    return \Drupal::translation()->translate( $content ); 
  }
}

