<?php

namespace Drupal\dsfr_twig_components\Twig;

use Drupal\dsfr_twig_components\Twig\DsfrComponents;

class PseudoComponents {

  /**
   * {@inheritdoc}
   */
  public static function cCode( string $text, array $attributes = [], string $language = 'twig' ) {

    //$build['#attached']['library'][] = 'dsfr_twig_components/code';

    if( $text != '' ) {

      $text = str_replace('<', '&lt;', $text );
      $text = str_replace('>', '&gt;', $text );
    }

    $content = [
      'text' => $text,
      'language' => 'language-'. $language
    ];

    // Attributes
    $pattern['class'] = ['fr-code'];
    $attributes['class'] = InternalTools::mergeClass( $pattern, $attributes );
    $attr_string = InternalTools::convertAttributes( $attributes );

    // Result
    return DsfrComponents::render( 'code', $content, $attributes, $attr_string );
}

  /**
   * Minor tools for writing code for documentation
   */
  public static function braces( $value ) { return '{&lbrace; '. $value .' &rbrace;}'; }
  public static function chevron( $value ) { return '&lt;'. $value .'&gt;'; }
  public static function chevrons( $value, $tag ) { 
    return self::chevron( $tag ). $value . self::chevron( '/'. $tag ); 
  }
  public static function codeTwig( $value ) { return '&lbrace;% '. $value .' %&rbrace;'; }
  public static function commentTwig( $value ) { return '&lbrace;# '. $value .' #&rbrace;'; }
  public static function varTwig( $value ) { return self::codeTwig('set '. $value); }

  /**
   * {@inheritdoc}
   */
  public static function cError( 
    string $content, 
    array $attributes = [], 
    $tra = true  ) {

    $data_config = [ 
      'text' => $content,
      'sm' => true,
      'type' => 'error',
      'close' => false
    ];

    return DsfrComponents::alert( $data_config, $attributes, $tra );
  }

  /**
   * {@inheritdoc}
   */
  public static function cIncorrect( string $msg ) {
    
    $msg = 'Error: ' . $msg;
    $content = InternalTools::tra( $msg );
    // This component must start from scratch to avoid "Fatal error: Allowed memory size"
    return '<div class="fr-alert fr-alert--error fr-alert--sm"><p>'. $content .'</p> </div>';
  }

  /**
   * {@inheritdoc}
   */
  public static function cInfo( string $text, array $attributes = [], $tra = false ) {

    $data_config = [ 
      'text' => $text,
      'sm' => true,
      'type' => 'info',
      'close' => false
    ];

    return dsfrComponents::alert( $data_config, $attributes, $tra );
  }

  /**
   * {@inheritdoc}
   */
  public static function cHl( string $content, array $attributes = [], $tra = false ) {

    $data_config['text'] = $content;
    return dsfrComponents::highlight( $data_config, $attributes );
  }

  /**
   * {@inheritdoc}
   */
  public static function cWarning( string $text, array $attributes = [], $tra = false ) {

    $data_config = [ 
      'text' => $text,
      'sm' => true,
      'close' => false
    ];

    return dsfrComponents::alert( $data_config, $attributes, $tra );
  }
}