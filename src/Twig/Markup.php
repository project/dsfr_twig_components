<?php

namespace Drupal\dsfr_twig_components\Twig;

/**
 * Pattern HTML
 */
class Markup {

  /**
   * Main HTML markup
   * 
   * @param string $content
   *   Content (accepts HTML)
   * @param array $attributes
   *   HTML tag attributes
   * @param string $tag
   *   HTML tag type
   * @param bool $tra
   *   Translation capabilities
   */
  public static function item( 
    string $content, 
    array $attributes = [], 
    string $tag = 'div', 
    bool $tra = false 
  ) {
    // Translate or not ?
    // ----------------------------------------------------------------------------------------- //
    $content = InternalTools::handleTra( $content, $tra ); 
    
    // Handle Attributes (array to string)
    // ----------------------------------------------------------------------------------------- //
    $attr = InternalTools::convertAttributes( $attributes ); 

    // Result
    // ----------------------------------------------------------------------------------------- //
    $single = [ 'hr', 'img' ];
    if( !in_array( $tag, $single) ) { $result = '<'. $tag . $attr .'>'. $content. '</'. $tag .'>'; } 
    elseif( $tag == 'img' ) { $result = '<'. $tag . $attr .' src='. $content .' />'; } 
    else { $result = '<'. $tag . $attr .' />'; }
    return $result;
  }

  /**
   * {@inheritdoc}
   * <a>$content</a>
   */
  public static function mA( string $content, array $attributes = [], bool $tra = false ) { 
    $attributes['href'] = ( array_key_exists('href', $attributes ) ) ? $attributes['href'] : '#';
    return Markup::item( $content, $attributes, 'a', $tra ); 
  }

  /**
   * {@inheritdoc}
   * <button>$content</button>
   */
  public static function mButton( string $content, array $attributes = [], bool $tra = false ) { 
    return Markup::item( $content, $attributes, 'button', $tra ); 
  }

  /**
   * {@inheritdoc}
   * <div>$content</div>
   */
  public static function mDiv( string $content, array $attributes = [], bool $tra = false ) { 
    return Markup::item( $content, $attributes, 'div', $tra ); 
  }

  /**
   * {@inheritdoc}
   * <hr /> (no content)
   */
  public static function mHr( array $attributes = [] ) { 
    return Markup::item( '', $attributes, 'hr' ); 
  }

  /**
   * {@inheritdoc}
   * <img src="$content" />
   */
  public static function mImg( string $content, array $attributes = [], bool $tra = false ) { 
    $attributes['src'] = ( array_key_exists( 'src', $attributes ) && $content == '' ) ? $attributes['src'] : $content;
    $attributes['alt'] = ( array_key_exists( 'alt', $attributes ) ) ? $attributes['alt'] : '';
    return Markup::item( $content, $attributes, 'img', $tra ); 
  }

  /**
   * {@inheritdoc}
   * <p>$content</p>
   */
  public static function mP( string $content, array $attributes = [], bool $tra = false ) {   
    return Markup::item( $content, $attributes, 'p', $tra ); 
  }
}