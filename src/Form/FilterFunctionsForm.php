<?php

namespace Drupal\dsfr_twig_components\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\dsfr_twig_components\Twig\Resources;

class FilterFunctionsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'filter_functions_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['search'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Filter'),
      '#placeholder' => $this->t('Enter a keyword to search for a function'),
      //'#required' => TRUE,
      '#ajax' => [
        'callback' => '::ajaxFilterCallback',
        'event' => 'keyup',
        'wrapper' => 'ajax-result',
      ],      
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  /**
   * AJAX callback function.
   */
  public function ajaxFilterCallback(array &$form, FormStateInterface $form_state) {

    $response = new AjaxResponse();
    $output = '';
    $search = $form_state->getValue('search');
    $functions = Resources::functionsReady();

    // Filter
    $filtered_data = array_filter($functions, function ($function) use ($search) {
      return stripos( $function['name'], $search) !== FALSE;
    });
    
    // Build    
    foreach ( $filtered_data as $icon ) {
      $output .= '
      <div class="fr-col-12 fr-col-lg-3">
        <a href="'.$icon['name'].'" class="fr-link">'. $icon['name'] .'</a>
      </div>';
    }
    // Render
    $response->addCommand(
      new HtmlCommand(
        '#ajax-result',
        $output
      )
    );
    return $response;
  }
}